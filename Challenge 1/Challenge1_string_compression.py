# -*- coding: utf-8 -*-
"""
Challenge 1 solution for the WhatBusy Intership challenge
"""

def string_compression(word):
    new=''
    count=1
    new+=word[0]
    
    for i in range(len(word)-1):
        if word[i] == word[i+1]:           
            count = count + 1
        else:
            if count > 1:
                new = new + str(count)

            new = new + word[i+1]
            count = 1


    if count > 1:
        new = new + str(count)
        
    return new


"""
Time complexity explanation of the string compression funtion 


def string_compression(word):
    new=''         						 # O(1)
    count=1								 # O(1)
    new+=word[0]						 # O(1)
    
    for i in range(len(word)-1): 		 # loop runs (n-1) times
        if word[i] == word[i+1]:        
            count = count + 1			 		# O(1)
        else:
            if count > 1:				
                new = new + str(count)	 		# O(1)

            new = new + word[i+1]		 		# O(1)
            count = 1							# O(1)


    if count > 1:
        new = new + str(count)			 # O(1)
        
    return new



summing up everythin we get
O(1) + O(1) + O(1) + O(1)(n-1) + O(1) = O(1) + O(n) = O(n)


thus this function has a linear time complexity 
the time increases linearly with the number of inputs

"""
   

print(string_compression('assssdddddfffffaaaaaa'))
